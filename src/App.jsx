import './App.css'
import {AbsoluteCenter, Box, Button} from "@chakra-ui/react";
import {useMyModalContext} from "./components/MyModal.jsx";
import React from "react";

function App() {
  const {handleModal} = useMyModalContext()

  return (
    <Box position='relative' h='100vh'>
      <AbsoluteCenter>
        <Button onClick={() => handleModal({productId: "0", open: true, isLoading: true})}>
          Open Modal
        </Button>
      </AbsoluteCenter>
    </Box>
  )
}

export default App
