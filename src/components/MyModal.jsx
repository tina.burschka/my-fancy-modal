import React, {useState, useContext, useMemo, createContext} from "react";
import {LoadingSpinner} from "./Loading.jsx";
import {MySubModal} from "./MySubModal.jsx";

export const MyModalContext = createContext()
export const useMyModalContext = () => useContext(MyModalContext)

export const MyModalProvider = ({ children }) => {
  const { isOpen, handleModal, productId, product, isLoading, onClose, props } = useMyModal();

  console.log("MyModalProvider isOpen", isOpen)
  console.log("MyModalProvider productId", productId)
  console.log("MyModalProvider product", product)
  console.log("MyModalProvider handleModal", handleModal)
  console.log("MyModalProvider onClose", onClose)
  console.log("MyModalProvider props", props)
  console.log("MyModalProvider isLoading", isLoading)

  return (
    <MyModalContext.Provider value={{ isOpen, handleModal, productId, product, isLoading, onClose, ...props }}>
      <MyModal />
      {children}
    </MyModalContext.Provider>
  );
};

export const MyModal = () => {
  const { isOpen, handleModal, product, productId, isLoading, onClose, ...props } = useMyModalContext()

  console.log("MyModal isOpen", isOpen)
  console.log("MyModal isLoading", isLoading)
  console.log("MyModal handleModal", handleModal)
  console.log("MyModal onClose", onClose)
  console.log("MyModal product", product)
  console.log("MyModal productId", productId)
  console.log("MyModal props", props)

  close = () => {
    handleModal("", false)
  }

  if (isLoading) {
    return (
      <LoadingSpinner />
    )
  }

  if (!product || !isOpen) {
    return null
  }

  return (
    <MySubModal product={product} isOpen={isOpen} onClose={onClose} {...props} />
  );
};

export const useMyModal = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [productId, setProductId] = useState(null);
  const [product, setProduct] = useState(null);
  const [props, setProps] = useState({})

  const loadProduct = (productId) => {
    console.log("loadProduct productId", productId)
    setProduct('product-' + productId)

    // simulate
    setTimeout(() => {
      setIsLoading(false)
      setIsOpen(true)
    }, 250)
  }

  const handleModal = ({productId = false, open = false, isLoading = false,  ...props}) => {
    console.log("handleModal productId", productId)
    console.log("handleModal open", open)
    console.log("handleModal isLoading", isLoading)
    console.log("handleModal props", props)

    if (productId) {
      setProductId(productId);
    }

    if (props) {
      setProps(props);
    }

    if (isLoading) {
      setIsLoading(isLoading)
      // fallback if i click to fast
      setTimeout(() => {
        setIsLoading(false)
      }, 5000)
    }

    setIsOpen(open);
  }

  const onClose = () => {
    setProductId(null)
    setIsOpen(false);
  }

  useMemo(() => {
    console.log("useMemo productId", productId)
    if (productId) {
      loadProduct(productId)
    }
  }, [productId]);

  return {
    isOpen,
    isLoading,
    onClose,
    handleModal,
    productId,
    product,
    props
  }
}