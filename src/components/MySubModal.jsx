import {
  Button,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay
} from "@chakra-ui/react";
import React from "react";
import {useMyModalContext} from "./MyModal.jsx";

export const MySubModal = ({product, isOpen, onClose, ...props}) => {
  const { handleModal } = useMyModalContext()

  console.log("MySubModal product", product)
  console.log("MySubModal isOpen", isOpen)
  console.log("MySubModal isOpen", onClose)
  console.log("MySubModal props", props)
  console.log("MySubModal handleModal", handleModal)

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Modal Title</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <h4>{ product }</h4>
        </ModalBody>

        <ModalFooter>
          <Button colorScheme='blue' mr={3} onClick={onClose}>
            Close
          </Button>
          <Button variant='ghost' onClick={() => handleModal({productId: Math.floor(Math.random() * 10), open: true, blah: "blubb", isLoading: true})}>Random number</Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  )
}