import {useState} from "react";
import {Spinner} from '@chakra-ui/react'

export const LoadingSpinner = () => {
  const {isLoading} = useLoading()

  console.log("LoadingSpinner isLoading", isLoading)

  if (!isLoading) {
    return null
  }

  return (
    <Spinner
      thickness='4px'
      speed='0.65s'
      emptyColor='gray.200'
      color='blue.500'
      size='xl'
    />
  )
}

export const useLoading = () => {
  const [isLoading, setIsLoading] = useState(true)

  return {
    isLoading,
    setIsLoading
  }
}